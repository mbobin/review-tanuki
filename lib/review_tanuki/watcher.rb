# frozen_string_literal: true

module ReviewTanuki
  class Watcher
    attr_reader :processor

    def initialize(processor, opts: {})
      @processor = processor
      @opts = opts
    end

    def execute
      each_marked_merge_request do |merge_request|
        emoji = my_emoji(merge_request)
        next unless emoji

        add_merge_request_to_trainee_issue(merge_request)
        delete_emoji(merge_request, emoji)
      end
    end

    private

    def each_marked_merge_request(&block)
      Gitlab.user_merge_requests(
        scope: 'all',
        my_reaction_emoji: reaction_emoji,
        order_by: 'created_at',
        sort: 'asc'
      ).auto_paginate(&block)
    end

    def reaction_emoji
      @opts[:reaction_emoji] || 'eyes'
    end

    def my_emoji(merge_request)
      Gitlab.award_emojis(merge_request.project_id, merge_request.iid, 'merge_request').auto_paginate do |emoji|
        return emoji if emoji.name == reaction_emoji && emoji.user.id == user_id
      end
    end

    def add_merge_request_to_trainee_issue(merge_request)
      return if dry_run?

      processor.issue.create_note(merge_request.references.full)
    end

    def delete_emoji(merge_request, emoji)
      return if dry_run?

      Gitlab.delete_award_emoji(merge_request.project_id, merge_request.iid, 'merge_request', emoji.id)
    end

    def user_id
      @user_id ||= processor.trainee_id
    end

    def dry_run?
      processor.dry_run?
    end
  end
end
