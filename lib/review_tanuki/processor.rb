# frozen_string_literal: true

module ReviewTanuki
  class Processor
    DEFAULT_OPTS = {
      project: 'gitlab-com/www-gitlab-com',
      client: Gitlab.client,
      dry_run: false
    }.freeze

    attr_reader :issue

    def initialize(issue_iid, opts: {})
      @opts = DEFAULT_OPTS.merge(opts)
      @issue = TraineeIssue.new(client, project, issue_iid)
    end

    def execute
      issue_notes do |note|
        next if note.system
        next if note.author.id != trainee_id

        project, mr_iid = extract_project_mr(note)
        next unless project

        process_mr(project, mr_iid, note)
      end
    end

    def trainee_id
      issue.trainee.id
    end

    def dry_run?
      @opts[:dry_run]
    end

    private

    def process_mr(project, mr_iid, note)
      mr = MergeRequest.new(client, project, mr_iid)

      logger.info("Processing #{mr.short_link} on note #{note.id}")

      changed = note.body.chomp != mr.review_body

      if changed
        logger.info("Updating note about #{mr.title}")

        issue.update_note(note.id, mr.review_body) unless dry_run?
      else
        logger.info('Nothing to update')
      end
    end

    def extract_project_mr(note)
      note_body = note.body.chomp

      new_mr = mr_link(note_body)
      return new_mr unless new_mr.nil?

      marker, _, link = note_body.split("\n", 4)
      return mr_link(link) if marker.chomp == MergeRequest::MARKER
    end

    def mr_link(note_body)
      match = %r{\A((?:[^/]+/){1,}[^/]+)!(\d+)\z}.match(note_body)

      return [match[1], match[2]] if match
    end

    def issue_notes
      issue.notes.auto_paginate do |note|
        yield note
      end
    end

    def client
      @opts[:client]
    end

    def project
      @opts[:project]
    end

    def logger
      @logger ||= Logger.new($stderr)
    end
  end
end
